# BKA Insights

## Getting started

For using the tool on your local workstation, first clone the repository, and then follow the steps below.

####Set up the database

Execute the scripts in `.../Api/InitialDatabaseScripts.txt` on your local MySQL database instance.

####Start the API

```
cd Api\
```

```
npm install
```

```
node Server.js
```

####Start the website

```
cd Website\
```

```
npm install
```

```
npm start
```

####Create a user
You can find the credentials for the admin user in `.../Api/InitialDatabaseScripts.txt`. Log in and add yourself as a user by clicking `Add/Update User` in the menu.
const jwt = require('jsonwebtoken');
const { SECRET_KEY } = require('./Constants');

const processAuthorizationHeader = (req, res, next) => {
  if (req.headers
    && req.headers.authorization
    && req.headers.authorization.split(' ')[0] === 'JWT') {
    jwt.verify(req.headers.authorization.split(' ')[1], SECRET_KEY, (err, decode) => {
      if (err) req.user = undefined;
      req.user = decode;
      next();
    });
  } else {
    req.user = undefined;
    next();
  }
};

const loginRequired = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.status(401).send();
  }
};

module.exports = {
  processAuthorizationHeader,
  loginRequired,
};

const express = require('express');
const bodyParser = require('body-parser');
const progressRouter = require('./Routers/ProgressRouter');
const bkaRequirementRouter = require('./Routers/BkaRequirementRouter');
const scoreTypeRouter = require('./Routers/ScoreTypeRouter');
const requirementResourceRouter = require('./Routers/RequirementResourceRouter');
const userRouter = require('./Routers/UserRouter');
const { processAuthorizationHeader } = require('./Authorization');
const { errorHandler } = require('./Error');

const app = express();
const PORT = process.env.PORT || 4001;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(processAuthorizationHeader);

const router = express.Router();
progressRouter(router);
bkaRequirementRouter(router);
scoreTypeRouter(router);
requirementResourceRouter(router);
userRouter(router);
app.use('/api', router);

app.use((err, req, res, next) => errorHandler(err, res)); // eslint-disable-line

app.listen(PORT);

console.log(`Api listening on port ${PORT}`); // eslint-disable-line

const conn = require('./Connection').create();

const selectAll = (callback) => {
  conn.query('SELECT * FROM bka.requirement_resource ORDER BY id;', callback);
};

module.exports = {
  selectAll,
};

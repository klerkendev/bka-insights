const conn = require('./Connection').create();

const selectHierarchy = (callback) => {
  const query = `SELECT bka_id, bka.name AS bka_name, 
      capability_level_id AS level_id, capl.name AS level_name, 
      capability_type_id AS type_id, capt.name AS type_name
    FROM bka.bka_requirement
      LEFT JOIN bka ON bka.id = bka_id
      LEFT JOIN capability_level AS capl ON capl.id = capability_level_id
      LEFT JOIN capability_type AS capt ON capt.id = capability_type_id
    GROUP BY bka_id, capability_level_id, capability_type_id
    ORDER BY bka_id, capability_level_id, capability_type_id;`;

  conn.query(query, callback);
};

module.exports = {
  selectHierarchy,
};

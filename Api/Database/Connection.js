const mysql = require('mysql');
const config = require('../Config/ServerConfig');

const create = () => mysql.createConnection({
  host: config.dbhost,
  user: config.dbuser,
  password: config.dbpassword,
  database: config.dbschema,
});

module.exports = {
  create,
};

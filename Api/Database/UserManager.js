const conn = require('./Connection').create();

const getUser = (email, callback) => {
  conn.query(`SELECT * FROM bka.user WHERE email = ${conn.escape(email)};`, callback);
};

const insertOrUpdateUser = (email, password, callback) => {
  const query = `INSERT INTO bka.user (email, password)
    VALUES (${conn.escape(email)}, ${conn.escape(password)})
    ON DUPLICATE KEY UPDATE password=${conn.escape(password)};`;

  conn.query(query, callback);
};

module.exports = {
  getUser,
  insertOrUpdateUser,
};

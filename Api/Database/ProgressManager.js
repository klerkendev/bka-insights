const conn = require('./Connection').create();

const select = (userId, callback) => {
  const query = `SELECT req.id AS id, bka_id, capability_level_id, capability_type_id, bka.name AS name, req.requirement AS requirement,
    IFNULL(prog.score, 0) AS score, IFNULL(prog.comment, '') AS comment
      FROM bka.bka_requirement AS req
      LEFT JOIN (SELECT * FROM user_progress WHERE user_id = ${conn.escape(userId)}) AS prog ON req.id = prog.bka_requirement_id
      LEFT JOIN bka.bka ON bka.id = req.bka_id;`;

  conn.query(query, callback);
};

const selectLevelStatus = (userId, callback) => {
  const query = `SELECT bka.id AS bka_id, level.id AS level_id, bka.name AS bka_name, level.name AS level_name,
    SUM(IFNULL(prog.score, 0)) AS score,
    COUNT(*) * max_req_score AS max_level_score
        FROM bka.bka_requirement AS req
        LEFT JOIN (SELECT * FROM user_progress WHERE user_id = ${conn.escape(userId)}) AS prog ON req.id = prog.bka_requirement_id
        LEFT JOIN bka.bka AS bka ON bka.id = req.bka_id
        LEFT JOIN bka.capability_level AS level ON level.id = req.capability_level_id
        LEFT JOIN (SELECT MAX(score) AS max_req_score FROM bka.score_type) AS score ON 1 = 1
    GROUP BY bka_id, level_id;`;

  conn.query(query, callback);
};

const insertOrUpdate = (progress, callback) => {
  const {
    userId, requirementId, score, comment,
  } = progress;

  const query = `INSERT INTO bka.user_progress (user_id, bka_requirement_id, score, comment)
    VALUES (
      ${conn.escape(userId)},
      ${conn.escape(requirementId)},
      ${conn.escape(score)},
      ${conn.escape(comment)})
    ON DUPLICATE KEY UPDATE
      score=${conn.escape(score)},
      comment=${conn.escape(comment)};`;

  conn.query(query, callback);
};

module.exports = {
  select,
  insertOrUpdate,
  selectLevelStatus,
};

const conn = require('./Connection').create();

const selectAll = (callback) => {
  conn.query('SELECT * FROM bka.score_type ORDER BY score DESC;', callback);
};

module.exports = {
  selectAll,
};

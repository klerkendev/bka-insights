const manager = require('../Database/ScoreTypeManager');
const { loginRequired } = require('../Authorization');
const { errorHandler } = require('../Error');

module.exports = (router) => {
  router.get('/scoretypes', loginRequired, (req, res) => {
    manager.selectAll((err, result) => {
      if (err) errorHandler(err, res);
      else res.send(result);
    });
  });
};

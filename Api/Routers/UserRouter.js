const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const manager = require('../Database/UserManager');
const { SECRET_KEY } = require('../Constants');
const { errorHandler } = require('../Error');

module.exports = (router) => {
  router.post('/user', (req, res) => {
    const { user, secret } = req.body;
    const { email, password } = user;

    if (email && password && secret === SECRET_KEY) {
      const saltRounds = 5;
      bcrypt.hash(password, saltRounds, (err, hash) => {
        manager.insertOrUpdateUser(email, hash, (error) => {
          if (error) errorHandler(error, res);
          res.send();
        });
      });
    } else {
      res.status(400).send();
    }
  });

  router.post('/login', (req, res) => {
    const { email, password } = req.body;

    manager.getUser(email, (err, result) => {
      if (err) errorHandler(err, res);
      else {
        const user = result[0];

        if (!user) {
          res.status(401).send();
        } else {
          bcrypt.compare(password, user.password, (pwErr, pwRes) => {
            if (pwErr) throw pwErr;

            if (pwRes) {
              res.json({
                id: user.id,
                isAdmin: user.is_admin === 1,
                token: jwt.sign({ email: result.email, id: result.id }, SECRET_KEY),
              }).send();
            } else {
              res.status(401).send();
            }
          });
        }
      }
    });
  });
};

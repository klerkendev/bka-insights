const manager = require('../Database/ProgressManager');
const { loginRequired } = require('../Authorization');
const { errorHandler } = require('../Error');

module.exports = (router) => {
  router.get('/progress/:id', loginRequired, (req, res) => {
    manager.select(req.params.id, (err, result) => {
      if (err) errorHandler(err, res);
      else res.send(result);
    });
  });

  router.get('/levelstatus/:id', loginRequired, (req, res) => {
    manager.selectLevelStatus(req.params.id, (err, result) => {
      if (err) errorHandler(err, res);
      else res.send(result);
    });
  });

  router.put('/progress/:id', loginRequired, (req, res) => {
    const progress = {
      userId: req.params.id,
      requirementId: req.body.requirement_id,
      score: req.body.score,
      comment: req.body.comment,
    };

    manager.insertOrUpdate(progress, (err, result) => {
      if (err) errorHandler(err, res);
      else res.send(result);
    });
  });
};

const manager = require('../Database/BkaRequirementManager');
const { loginRequired } = require('../Authorization');
const { errorHandler } = require('../Error');

module.exports = (router) => {
  router.get('/requirementhierarchy', loginRequired, (req, res) => {
    manager.selectHierarchy((err, result) => {
      if (err) errorHandler(err, res);
      else res.send(result);
    });
  });
};

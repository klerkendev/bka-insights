const errorHandler = (err, res) => {
  console.error(err); // eslint-disable-line
  res.status(500).send();
};

module.exports = {
  errorHandler,
};

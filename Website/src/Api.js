import { LOCAL_STORAGE_KEY, SECRET_KEY } from './Constants';

const addAuthenticationHeader = (addHeaders) => {
  const headers = { ...addHeaders };
  const token = localStorage.getItem(LOCAL_STORAGE_KEY);

  if (token !== null) {
    headers.Authorization = `JWT ${localStorage.getItem(LOCAL_STORAGE_KEY)}`;
  }

  return headers;
};

const processFetch = (res, resolve, reject) => {
  if (!res.ok) reject(res);
  else { resolve(res.json()); }
};

export const getProgress = userId => new Promise((resolve, reject) => {
  fetch(`/api/progress/${userId}`, { method: 'GET', headers: addAuthenticationHeader() })
    .then(res => processFetch(res, resolve, reject));
});

export const getDashboardStatus = userId => new Promise((resolve, reject) => {
  fetch(`/api/levelstatus/${userId}`, { method: 'GET', headers: addAuthenticationHeader() })
    .then(res => processFetch(res, resolve, reject));
});

export const getScoreTypes = () => new Promise((resolve, reject) => {
  fetch('/api/scoretypes', { method: 'GET', headers: addAuthenticationHeader() })
    .then(res => processFetch(res, resolve, reject));
});

export const getResources = () => new Promise((resolve, reject) => {
  fetch('/api/resources', { method: 'GET', headers: addAuthenticationHeader() })
    .then(res => processFetch(res, resolve, reject));
});

export const getRequirementHierarchy = () => new Promise((resolve, reject) => {
  fetch('/api/requirementhierarchy', { method: 'GET', headers: addAuthenticationHeader() })
    .then(res => processFetch(res, resolve, reject));
});

export const updateProgress = (userId, requirementId, score, comment) =>
  new Promise((resolve, reject) => {
    fetch(`/api/progress/${userId}`, {
      method: 'PUT',
      headers: addAuthenticationHeader({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({
        requirement_id: requirementId,
        score,
        comment,
      }),
    }).then(res => processFetch(res, resolve, reject));
  });

export const login = (email, password) =>
  new Promise((resolve, reject) => {
    fetch('/api/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password,
      }),
    }).then(res => processFetch(res, resolve, reject));
  });

export const addUser = (email, password) =>
  new Promise((resolve, reject) => {
    fetch('/api/user', {
      method: 'POST',
      headers: addAuthenticationHeader({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({
        user: {
          email,
          password,
        },
        secret: SECRET_KEY,
      }),
    }).then((res) => {
      if (!res.ok) reject(res);
      else resolve();
    });
  });

import React, { Component } from 'react';
import { Form, Input, Icon, Button } from 'antd';
import PropTypes from 'prop-types';
import sha256 from 'js-sha256';
import { login } from '../Api';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const email = e.target.email.value;
    const password = sha256(e.target.password.value);

    login(email, password)
      .then(authData => this.props.onLoginSuccess(
        authData.id,
        email,
        authData.token,
        authData.isAdmin,
      ))
      .catch(() => this.props.messageHandler('Failed login attempt', true));
  }

  render() {
    return (
      <section>
        <div className="credentials">
          <Form onSubmit={this.handleSubmit} >
            <Input
              name="email"
              prefix={<Icon type="user" />}
              placeholder="Email"
            />
            <Input
              name="password"
              prefix={<Icon type="lock" />}
              type="password"
              placeholder="Password"
            />
            <Button
              htmlType="submit"
            >Log in
            </Button>
          </Form>
        </div>
      </section>
    );
  }
}

Login.propTypes = {
  onLoginSuccess: PropTypes.func.isRequired,
  messageHandler: PropTypes.func.isRequired,
};

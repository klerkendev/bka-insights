import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Progress, Icon } from 'antd';
import uniqueById from '../Util/ArrayUtil';
import ScoreInput from '../Components/ScoreInput';
import CommentField from '../Components/CommentField';
import HierarchySelect from '../Components/HierarchySelect';
import ResourceSet from '../Components/ResourceSet';
import { getProgress, getScoreTypes, getResources, updateProgress, getDashboardStatus } from '../Api';

const calculateLevelPercentage = (level) => {
  if (level.score === 0) return 0;
  return Math.round((level.score / level.max_level_score) * 100);
};

export default class Insights extends Component {
  constructor() {
    super();

    this.state = {
      progress: [],
      selectedProgress: [],
      scoreTypes: [],
      resources: [],
      dashboardStatus: [],
      isDashboardOpen: true,
    };

    this.setScoreTypes();
    this.setResources();

    this.selectProcess = this.selectProcess.bind(this);
    this.openDashboard = this.openDashboard.bind(this);
  }

  componentDidMount() {
    this.setProgress();
    this.setDashboardStatus();
  }

  setScoreTypes() {
    getScoreTypes()
      .then(scoreTypes => this.setState({ scoreTypes }))
      .catch(error => this.props.messageHandler(error.statusText, true));
  }

  setProgress() {
    getProgress(this.props.userId)
      .then(progress => this.setState({ progress }))
      .catch(error => this.props.messageHandler(error.statusText, true));
  }

  setDashboardStatus() {
    getDashboardStatus(this.props.userId)
      .then((dashboardStatus) => {
        const nested = uniqueById(dashboardStatus.map(status => (
          {
            id: status.bka_id,
            bka_name: status.bka_name,
            levels: dashboardStatus.filter(level => level.bka_id === status.bka_id),
          }
        )));

        this.setState({ dashboardStatus: nested });
      })
      .catch(error => this.props.messageHandler(error.statusText, true));
  }

  setResources() {
    getResources()
      .then(resources => this.setState({ resources }))
      .catch(error => this.props.messageHandler(error.statusText, true));
  }

  getProgressIndex(requirementId) {
    return this.state.progress.findIndex(prog => prog.id === requirementId);
  }

  updateScore(progressId, comment) {
    return (score) => {
      updateProgress(this.props.userId, progressId, score, comment)
        .then(() => {
          const progress = [...this.state.progress];
          progress[this.getProgressIndex(progressId)].score = score;
          this.setState({ progress });
        })
        .catch(error => this.props.messageHandler(error.statusText, true));
    };
  }

  updateComment(progressId, score) {
    return (comment) => {
      updateProgress(this.props.userId, progressId, score, comment)
        .then(() => {
          const progress = [...this.state.progress];
          progress[this.getProgressIndex(progressId)].comment = comment;
          this.setState({ progress });
        })
        .catch(error => this.props.messageHandler(error.statusText, true));
    };
  }

  selectProcess({ bka, level, type }) {
    const selectedProgress = this.state.progress.filter(i => i.bka_id === bka
      && i.capability_level_id === level
      && i.capability_type_id === type);

    this.setState({ selectedProgress, isDashboardOpen: false });
  }

  getRequirementResources(requirementId) {
    return this.state.resources
      .filter(resource => resource.requirement_id === requirementId)
      .map((reqResource => ({
        id: reqResource.id,
        title: reqResource.title,
        url: reqResource.url,
      })));
  }

  openDashboard() {
    this.setDashboardStatus();
    this.setState({ isDashboardOpen: true });
  }

  showProgress() {
    return (
      this.state.selectedProgress.map(prog => (
        <tbody key={prog.id}>
          <tr>
            <td width="50px" rowSpan="2">{<ScoreInput
              scoreTypes={this.state.scoreTypes}
              selected={prog.score}
              onChange={this.updateScore(prog.id, prog.comment)}
            />}
            </td>
            <td>
              {prog.requirement}
            </td>
          </tr>
          <tr>
            <td>
              {<ResourceSet resources={this.getRequirementResources(prog.id)} />}
              {<CommentField
                textValue={prog.comment}
                onSubmit={this.updateComment(prog.id, prog.score)}
              />}
            </td>
          </tr>
        </tbody>
      ))
    );
  }

  showDashboard() {
    return (
      this.state.dashboardStatus.map(status => (
        <tbody key={status.id}>
          <tr>
            <td>
              <span className="dasboard_title">{status.bka_name}</span>
            </td>
            {status.levels.map(level => (
              <td key={level.level_id} className="dashboard_status">
                <span>{level.level_name}</span>
                <Progress
                  key={level.level_id}
                  percent={calculateLevelPercentage(level)}
                />
              </td>
              ))
            }
          </tr>
        </tbody>
      ))
    );
  }

  showNavigation() {
    return (
      <div>
        <span onClick={() => this.setState({ isDashboardOpen: false })}>
          <Icon type="line-chart" /> Progress
        </span>
        <span onClick={this.openDashboard}>
          <Icon type="dashboard" /> Dashboard
        </span>
      </div>
    );
  }

  render() {
    return (
      <section>
        <div className="tbl-header">
          <table>
            <thead>
              <tr>
                <th>{
                  <HierarchySelect
                    onChange={this.selectProcess}
                    messageHandler={this.props.messageHandler}
                  />}
                </th>
                <th>
                  {this.state.selectedProgress.length > 0 ? this.showNavigation() : null}
                </th>
              </tr>
            </thead>
          </table>
        </div>
        <div className="tbl-content">
          <table>
            {this.state.isDashboardOpen ? this.showDashboard() : this.showProgress()}
          </table>
        </div>
      </section>
    );
  }
}

Insights.propTypes = {
  userId: PropTypes.number.isRequired,
  messageHandler: PropTypes.func.isRequired,
};

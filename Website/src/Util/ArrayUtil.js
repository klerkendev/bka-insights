export default function uniqueById(array) {
  return array.filter((val, index, arr) => arr.map(x => x.id).indexOf(val.id) === index);
}

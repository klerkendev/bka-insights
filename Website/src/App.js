import React, { Component } from 'react';
import moment from 'moment';
import { Icon } from 'antd';
import Insights from './Pages/Insights';
import Login from './Pages/Login';
import UserPanel from './Components/UserPanel';
import './App.css';
import { LOCAL_STORAGE_KEY } from './Constants';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      messages: [],
    };

    this.handleMessage = this.handleMessage.bind(this);
    this.handleLoginSuccess = this.handleLoginSuccess.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleMessage(text, isError = false) {
    const messages = [...this.state.messages];
    const id = messages.length + 1;

    messages.unshift({
      id,
      timestamp: moment(),
      text,
      isError,
    });

    this.setState({ messages });
  }

  closeMessage(id) {
    const messages = [...this.state.messages].filter(e => e.id !== id);
    this.setState({ messages });
  }

  handleLoginSuccess(id, email, token, isAdmin) {
    localStorage.setItem(LOCAL_STORAGE_KEY, token);

    this.setState({
      user: {
        id,
        email,
        token,
        isAdmin,
      },
    });
  }

  handleLogout() {
    localStorage.removeItem(LOCAL_STORAGE_KEY);

    this.setState({ user: {} });
  }

  showMessages() {
    if (this.state.messages.length > 0) {
      return (
        <table className="tbl-messages">
          <tbody>
            {this.state.messages.map(message => (
              <tr className={message.isError ? 'error' : null} key={message.id}>
                <td><span>{message.timestamp.format('YYYY-MM-DD HH:mm:ss')}</span></td>
                <td>
                  <Icon
                    type="close"
                    onClick={() => this.closeMessage(message.id)}
                  /><span>{message.text}</span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      );
    }
    return null;
  }

  showPage() {
    if (this.state.user.id) {
      return (<Insights
        userId={this.state.user.id}
        messageHandler={this.handleMessage}
      />);
    }
    return (<Login
      onLoginSuccess={this.handleLoginSuccess}
      messageHandler={this.handleMessage}
    />);
  }

  showUserPanel() {
    return (
      <UserPanel
        user={this.state.user}
        logoutHandler={() => this.handleLogout}
        messageHandler={this.handleMessage}
      />
    );
  }

  render() {
    return (
      <div>
        {this.showMessages()}
        {this.showUserPanel()}
        {this.showPage()}
      </div>
    );
  }
}

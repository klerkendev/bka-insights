import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Menu, Dropdown, Icon } from 'antd';

export default class ScoreInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scoreTypes: props.scoreTypes,
      selected: props.selected,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange({ key }) {
    const keyInt = parseInt(key, 10);
    this.setState({ selected: keyInt });
    this.props.onChange(keyInt);
  }

  getDropdownMenu() {
    return (
      <Menu onClick={this.onChange}>
        {this.state.scoreTypes.map(type => (
          <Menu.Item key={type.score}>{<span><Icon className={`score_indicator_${type.score}`} type="minus" /><span> {type.description}</span></span>}</Menu.Item>
        ))}
      </Menu>
    );
  }

  render() {
    return (
      <div>
        <Dropdown overlay={this.getDropdownMenu()}>
          <Button className={`score_indicator_${this.state.selected}`} shape="circle" icon="line-chart" />
        </Dropdown>
      </div>
    );
  }
}

ScoreInput.propTypes = {
  selected: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  scoreTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
};

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Tag, Icon } from 'antd';

export default class CommentField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: props.textValue,
      isEditable: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    e.preventDefault();
    this.setState({ textValue: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state.textValue);
    this.setState({ isEditable: false });
  }

  getInput() {
    return (
      <Input
        className="input_comment"
        autoFocus
        type="text"
        size="small"
        value={this.state.textValue}
        onChange={this.onChange}
        onBlur={this.onSubmit}
        onPressEnter={this.onSubmit}
      />
    );
  }

  getComment() {
    return (
      <Tag
        className="comment"
        onClick={() => this.setState({ isEditable: true })}
      >
        <Icon type="bars" /> {this.state.textValue}
      </Tag>
    );
  }

  getAddButton() {
    return (
      <Tag
        className="add_comment"
        onClick={() => this.setState({ isEditable: true })}
      >
        <Icon type="bars" />
      </Tag>
    );
  }

  render() {
    if (this.state.isEditable) return this.getInput();
    return this.state.textValue ? this.getComment() : this.getAddButton();
  }
}

CommentField.propTypes = {
  textValue: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

import React, { Component } from 'react';
import { Icon, Form, Input, Button } from 'antd';
import sha256 from 'js-sha256';
import PropTypes from 'prop-types';
import { addUser } from '../Api';

export default class UserPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputUser: false,
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
  }

  handleAddUser(e) {
    e.preventDefault();
    const email = e.target.email.value;
    const password = sha256(e.target.password.value);

    addUser(email, password)
      .then(() => {
        this.props.messageHandler(`Added/updated user '${email}'`);
        this.handleClose();
      })
      .catch(() => this.props.messageHandler(`Failed to add/update user '${email}'`, true));
  }

  handleClose() {
    this.setState({ inputUser: false });
  }

  showPanel() {
    if (this.props.user.id) {
      return (
        <div>
          <table className="tbl-auth">
            <tbody>
              <tr>
                <td>
                  <span><Icon type="user" /> {this.props.user.email}</span>
                </td>
                <td className="userpanel_actions">
                  {this.adminOnly(this.showAddButton())}
                  <span
                    onClick={this.props.logoutHandler()}
                  ><Icon type="logout" /> Logout
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
          {this.adminOnly(this.showUserManagement())}
        </div>
      );
    }
    return null;
  }

  adminOnly(component) {
    if (this.props.user.isAdmin) {
      return component;
    }
    return null;
  }

  showAddButton() {
    return (
      <span
        onClick={() => this.setState({ inputUser: !this.state.inputUser })}
      ><Icon type="user-add" /> Add/Update User
      </span>
    );
  }

  showUserManagement() {
    if (this.state.inputUser) {
      return (
        <div className="credentials">
          <Form onSubmit={this.handleAddUser} >
            <Input
              name="email"
              placeholder="Email"
            />
            <Input
              name="password"
              placeholder="Password"
            />
            <Button
              htmlType="submit"
            >Add/Update User
            </Button>
          </Form>
          <Icon onClick={this.handleClose} className="close" type="up" />
        </div>);
    }
    return null;
  }

  render() {
    return this.showPanel();
  }
}

UserPanel.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    email: PropTypes.string,
    isAdmin: PropTypes.bool,
  }).isRequired,
  logoutHandler: PropTypes.func.isRequired,
  messageHandler: PropTypes.func.isRequired,
};

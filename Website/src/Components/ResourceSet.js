import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tag, Tooltip, Icon } from 'antd';

const titleMaxLength = 30;

const titlePreviewLength = titleMaxLength - 10;

const hasFittingTitle = resource => resource.title.length <= titleMaxLength;

const isLink = resource => resource.url !== null;

const createLink = (title, url) =>
  <div><Icon type="arrow-right" /> <a href={url} target="_blank">{title}</a></div>;

const getResourceTag = (resource) => {
  const displayTitle = hasFittingTitle(resource) ? resource.title : `${resource.title.substring(0, titlePreviewLength)}...`;

  return (
    <Tag key={resource.id}>
      {isLink(resource) ? createLink(displayTitle, resource.url) : displayTitle}
    </Tag>
  );
};

const createResource = (resource) => {
  const tag = getResourceTag(resource);

  if (hasFittingTitle(resource)) {
    return tag;
  }
  return (
    <Tooltip key={resource.id} overlayStyle={{ fontSize: '12px' }} placement="bottomLeft" title={resource.title}>
      {tag}
    </Tooltip>
  );
};

export default class ResourceSet extends Component {
  render() {
    return this.props.resources.map(resource =>
      createResource(resource));
  }
}

ResourceSet.propTypes = {
  resources: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    url: PropTypes.string,
  })).isRequired,
};

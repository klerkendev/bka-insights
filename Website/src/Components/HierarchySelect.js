import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Cascader, Button } from 'antd';
import { getRequirementHierarchy } from '../Api';
import uniqueById from '../Util/ArrayUtil';

const constructKey = (bkaId, levelId = 0, typeId = 0) => `${bkaId}-${levelId}-${typeId}`;

const destructKey = (key) => {
  const ids = key.split('-');
  return {
    bka: parseInt(ids[0], 10),
    level: parseInt(ids[1], 10),
    type: parseInt(ids[2], 10),
  };
};

export default class HierarchySelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hierarchy: [],
      buttonText: 'Select your BKA',
    };

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.setRequirementHierarchy();
  }

  setRequirementHierarchy() {
    getRequirementHierarchy()
      .then(hierarchy => this.setState({ hierarchy }))
      .catch(error => this.props.messageHandler(error.statusText, true));
  }

  getUniqueBkas() {
    return uniqueById(this.state.hierarchy.map(x => ({
      id: x.bka_id,
      name: x.bka_name,
    })));
  }

  getUniqueCapabilityLevels(bkaId) {
    return uniqueById(this.state.hierarchy
      .filter(i => i.bka_id === bkaId)
      .map(x => ({
        id: x.level_id,
        name: x.level_name,
      })));
  }

  getUniqueCapabilityTypes(bkaId, levelId) {
    return uniqueById(this.state.hierarchy
      .filter(i => i.bka_id === bkaId
        && i.level_id === levelId)
      .map(x => ({
        id: x.type_id,
        name: x.type_name,
      })));
  }

  constructData() {
    return this.getUniqueBkas().map(bka => ({
      value: constructKey(bka.id),
      label: bka.name,
      children: this.getUniqueCapabilityLevels(bka.id).map(level => ({
        value: constructKey(bka.id, level.id),
        label: level.name,
        children: this.getUniqueCapabilityTypes(bka.id, level.id).map(type => ({
          value: constructKey(bka.id, level.id, type.id),
          label: type.name,
        })),
      })),
    }));
  }

  onChange(value, selected) {
    this.setState({
      buttonText: selected.map(o => o.label).join(' - '),
    });
    this.props.onChange(destructKey(value[2]));
  }

  render() {
    return (
      <div>
        <Cascader
          expandTrigger="hover"
          size="small"
          options={this.constructData()}
          onChange={this.onChange}
        >
          <Button size="small">
            {this.state.buttonText}
          </Button>
        </Cascader>
      </div>
    );
  }
}

HierarchySelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  messageHandler: PropTypes.func.isRequired,
};
